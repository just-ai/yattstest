import com.google.protobuf.ByteString;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Metadata;
import io.grpc.Status;
import io.grpc.stub.MetadataUtils;
import io.grpc.stub.StreamObserver;
import yandex.cloud.ai.stt.v2.SttServiceGrpc;
import yandex.cloud.ai.stt.v2.TtsService;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.Comparator;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class App {
    //Записать OPUS arecord   -c   1   -r  48000  -twav  -  |  opusenc  --bitrate  96  - audio.opus
    //Записать LPCM arecord   -c   1   -r  16000  -f S16_LE -t raw audio.lpcm
    //Отправить из файла java -jar target/ysr-test-1.0-SNAPSHOT-jar-with-dependencies.jar FOLDERID IAMTOKEN audio.opus
    //Отправить с микрофона OPUS arecord   -c   1   -r  48000  -twav  -  |  opusenc  --bitrate  96  - - | java -jar target/ysr-test-1.0-SNAPSHOT-jar-with-dependencies.jar FOLDERID IAMTOKEN
    //Отправить с микрофона LPCM arecord   -c   1   -r  16000  -f S16_LE -t raw - | java -jar target/ysr-test-1.0-SNAPSHOT-jar-with-dependencies.jar FOLDERID IAMTOKEN

    public static void main(String[] args) throws Exception {
        String folderId = args[0];
        String iamToken = args[1];
        String fileName = args.length >= 3 ? args[2] : null;

        Metadata metadata = new Metadata();
        Metadata.Key<String> authorizationHeader = Metadata.Key.of("authorization", Metadata.ASCII_STRING_MARSHALLER);
        metadata.put(authorizationHeader, "Bearer " + iamToken);

        TtsService.RecognitionSpec spec = TtsService.RecognitionSpec.newBuilder()
                .setLanguageCode("ru-RU")
                .setProfanityFilter(false)
                .setAudioEncoding(TtsService.RecognitionSpec.AudioEncoding.LINEAR16_PCM)
                .setSampleRateHertz(16000)
                .setPartialResults(false)
                .setSingleUtterance(false)
                .build();

        TtsService.RecognitionConfig config = TtsService.RecognitionConfig.newBuilder()
                .setSpecification(spec)
                .setFolderId(folderId)
                .build();

        ManagedChannel channel = ManagedChannelBuilder
                .forAddress("stt.api.cloud.yandex.net", 443)
                .build();

        final CountDownLatch finishLatch = new CountDownLatch(1);

        SttServiceGrpc.SttServiceStub stub = SttServiceGrpc.newStub(channel);
        stub = MetadataUtils.attachHeaders(stub, metadata);

        StreamObserver<TtsService.StreamingRecognitionRequest> stream = stub.streamingRecognize(new StreamObserver<TtsService.StreamingRecognitionResponse>() {
            public void onNext(TtsService.StreamingRecognitionResponse value) {
                System.out.println();
                System.out.println("--------------------------------");
                System.out.println();
                System.out.println("RESULTS");

                for (TtsService.SpeechRecognitionChunk result : value.getChunksList()) {
                    System.out.println("TEXT:");
                    result.getAlternativesList().stream()
                            .max(Comparator.comparingDouble(TtsService.SpeechRecognitionAlternative::getConfidence))
                            .map(TtsService.SpeechRecognitionAlternative::getText)
                            .ifPresent(System.out::println);

                    System.out.println("FINAL: " + result.getFinal());
                    System.out.println("TIME: " + Instant.now());
                }
            }

            public void onError(Throwable throwable) {
                Status status = Status.fromThrowable(throwable);
                System.out.println("Error. Code: " + status.getCode() + ". Message: " + status.getDescription());
                finishLatch.countDown();
            }

            public void onCompleted() {
                System.out.println("Stop");
                finishLatch.countDown();
            }
        });


        stream.onNext(TtsService.StreamingRecognitionRequest.newBuilder().setConfig(config).build());

        if (fileName != null && !fileName.isEmpty()) {
            sendFromFile(stream, fileName);
        }
        else {
            sendFromStdIn(stream);
        }

        stream.onCompleted();

        finishLatch.await(1, TimeUnit.MINUTES);
    }

    private static byte[] BUFFER = new byte[1024 * 1024];
    private static long TIME_LIMIT_IN_MS = 60_000;

    private static void sendFromStdIn(StreamObserver<TtsService.StreamingRecognitionRequest> stream) throws Exception {

        long start = System.currentTimeMillis();

        do {
            int available = System.in.available();
            if (available > 0) {
                int read = System.in.read(BUFFER, 0, available);

                ByteString audioContent = ByteString.copyFrom(BUFFER, 0, read);

                stream.onNext(TtsService.StreamingRecognitionRequest.newBuilder()
                        .setAudioContent(audioContent)
                        .build());
            }
            else {
                Thread.sleep(5);
            }
        }
        while (System.currentTimeMillis() - start < TIME_LIMIT_IN_MS);
    }

    private static void sendFromFile(
            StreamObserver<TtsService.StreamingRecognitionRequest> stream, String fileName) throws Exception {
        ByteBuffer byteBuffer = ByteBuffer.wrap(getAudioBytes(fileName));

        while (byteBuffer.remaining() > 0) {
            int bytesToCopy = Math.min(2048, byteBuffer.remaining());

            ByteString audioContent = ByteString.copyFrom(byteBuffer, bytesToCopy);

            stream.onNext(TtsService.StreamingRecognitionRequest.newBuilder()
                    .setAudioContent(audioContent)
                    .build());

            Thread.sleep(100);
        }
    }

    private static byte[] getAudioBytes(String fileName) throws IOException {
        return Files.readAllBytes(Paths.get(fileName));
    }
}
